export interface CreateUser {
  firstname: String;
  lastname: String;
  email: String;
  password: String;
  urlPhoto: String;
}

