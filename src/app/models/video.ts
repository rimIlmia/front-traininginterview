export interface Video {
  idVideo: number;
  titreVideo: String;
  nomVideo: String;
  extension: String;
  lastName: String;
  idUser: Number;
  likes: number;
}
