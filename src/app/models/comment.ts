export interface Comment {
    idComment?: number;
    content?: String;
    idVideo?: number;
    idUser?:number;
    date?:Date;
}

