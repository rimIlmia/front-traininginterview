export interface User {
  idUser?:String
  email?: String;
  password?: String;
  firstname: String;
  lastname: String;
  age?: Number;
  urlPhoto?: String;
  metier?: String;
  linkedin?: String;
  facebook?: String;
  description?:String;
  pays?:String;
  ville?:String
}
