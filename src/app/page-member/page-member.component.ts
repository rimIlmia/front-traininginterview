import { Component, OnInit } from "@angular/core";
import { ProfilService } from "../services/profils/profil.service";
import { Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-page-member",
  templateUrl: "./page-member.component.html",
  styleUrls: ["./page-member.component.css"]
})
export class PageMemberComponent implements OnInit {
  private routeSub: Subscription;
  user: any;
  idUser: any;
  constructor(private us: ProfilService, private route: ActivatedRoute) {}

  ngOnInit() {
    // initialisation card-profil and list publication 
    this.routeSub = this.route.params.subscribe(params => {
      this.idUser = params["id"];
      this.us.getUserById(params["id"]).subscribe(data => {
        this.user = data["user"];
        console.log("***************",data)
        console.log(this.user);
      });
    });
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
