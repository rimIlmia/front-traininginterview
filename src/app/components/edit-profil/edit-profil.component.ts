import { environment } from "./../../../environments/environment";
import { User } from "./../../models/user";
import { USER_INFO } from "./../../common/app.util";
import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpEventType } from "@angular/common/http";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ProfilService } from "src/app/services/profils/profil.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-edit-profil",
  templateUrl: "./edit-profil.component.html",
  styleUrls: ["./edit-profil.component.css"]
})
export class EditProfilComponent implements OnInit {
  pays = "France";
  imageURL = environment.imagesURL;
  apiURL = environment.apiURl;
  ville = "";
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  valueProgress = 0;
  userEditForm: FormGroup;
  submitted = false;
  firstname = "";
  lastname = "";
  metier = "";
  facebook = "";
  linkedin = "";
  description = "";
  namePhoto = "";
  activate = true;
  extension = "";
  extensionValidator = false;
  sizeVideo: any = null;
  messageSize = "";
  messageExtension = "";
  validatorPhoto = true;
  usee: any;
  user: User;
  photo: string;
  fullPathname: string;

  constructor(
    private httpClient: HttpClient,
    private formBuilder: FormBuilder,
    private us: ProfilService,
    private router: Router
  ) {}
  // chargement du fichier
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    console.log("thisfile" + this.fileData);
    this.preview();
    this.valueProgress = 0;
    this.namePhoto = this.fileData.name;
    //size file mo
    this.sizeVideo = this.fileData.size / 1048576;
    this.extension = this.namePhoto.split(".").pop();
    this.messageExtension = "";
    this.validationExtensionPhoto();
    if (this.extensionValidator === true) {
      this.messageExtension = "";
    }

    if (this.validationSizePhoto() === true) {
      this.messageSize = "";
    }
  }

  validationExtensionPhoto() {
    console.log("ex:" + this.extension);
    if (
      this.extension === "jpg" ||
      this.extension === "jpeg" ||
      this.extension === "png"
    ) {
      this.extensionValidator = true;
    }

    console.log(this.extensionValidator);
  }
  validationSizePhoto() {
    if (this.sizeVideo === 0 || this.sizeVideo > 25) {
      return false;
    } else return true;
  }

  onSubmit() {
    this.user = {
      idUser: JSON.parse(localStorage.getItem("UserInfo")).idUser,
      firstname: this.firstname,
      lastname: this.lastname,
      metier: this.metier,
      pays: this.pays,
      ville: this.ville,
      linkedin: this.linkedin,
      facebook: this.facebook,
      description: this.description
    };
    this.submitted = true;
    // cas utilisateur change sa photo de profil
    if (this.namePhoto !== "") {
      // verification de l'extension du fichier

      this.validationExtensionPhoto();

      if (this.extensionValidator === false && this.submitted === true) {
        this.messageExtension = "Extensions photo acceptées sont: jpg et png";
      }
      //verification taille du fichier
      if (this.validationSizePhoto() === false && this.submitted === true) {
        this.messageSize =
          "La taille de votre photo doit etre inferieur à 15MO ";
      }
      const formData = new FormData();
      formData.append("myFile", this.fileData);

      // formulaire n'est pas valide
      if (
        this.userEditForm.invalid ||
        this.namePhoto === "" ||
        this.extensionValidator === false ||
        this.validationSizePhoto() === false
      ) {
        console.log("formulaire non valide");
        return;
      }
      // formulaire valide
      else {
        console.log("formulaire valide");
        //appel de l'api à la fonction upload qui utilise multer

        this.httpClient
          .post(this.apiURL+"user/upload/photo", formData, {
            reportProgress: true,
            observe: "events"
          })
          .subscribe(events => {
            if (events.type === HttpEventType.UploadProgress) {
              //calcul pour la progressbar
              this.fileUploadProgress =
                Math.round((events.loaded / events.total) * 100) + "%";
              this.valueProgress = Math.round(
                (events.loaded / events.total) * 100
              );
              if (this.valueProgress === 100) {
                this.valueProgress = 0;
              }
            } else if (events.type === HttpEventType.Response) {
              this.fileUploadProgress = "";
              this.valueProgress = 0;
              if (this.valueProgress === 100) {
                this.valueProgress = 0;
              }
              //Quand la photo est bien uploadé stoké ses données dans la BD
              this.user.urlPhoto = events.body["filename"];
              this.us.updateProfile(this.user).subscribe(data => {
                console.log(data);
                if (data["success"]) {
                  this.updateLocalStorage();
                  this.router.navigate(["/profil"]);
                }
              });
              // let user = {
              //   firstname: this.firstname,
              //   lastname: this.lastname,
              //   metier: this.metier,
              //   linkedin: this.linkedin,
              //   facebook: this.facebook,
              //   description: this.description,
              //   photoUrl: events.body["filename"]
              // };
              console.log(events.body["filename"]);
              // let publication = {
              //   titreVideo: this.question,
              //   nomVideo: events.body["filename"]
              //     .split(".")
              //     .slice(0, -1)
              //     .join("."),
              //   idUser: JSON.parse(localStorage.getItem("UserInfo")).idUser,
              //   extension: this.extension
              // };
              // appel du service publication et ajout de la publication à la BD
              // this.us
              //   .addPublication(publication)
              //   .subscribe(data => console.log(data["msg"]));
            }
          });
      }
    }
    // cas utilisateur garde sa photo de profil
    else {
      if (this.userEditForm.invalid) {
        console.log("formulaire non valide");
        return;
      } else {
        console.log("form valide ss photo");

        this.us.updateProfile(this.user).subscribe(data => {
          console.log(data);
          if (data["success"]) this.updateLocalStorage();
          this.router.navigate(["/profil"]);
        });
      }
    }
  }
  //MAJ localStorage
  updateLocalStorage() {
    let infoUser = JSON.parse(localStorage.getItem("UserInfo"));
    infoUser.pays = this.pays;
    infoUser.firstname = this.firstname;
    infoUser.lastname = this.lastname;
    infoUser.metier = this.metier;
    infoUser.ville = this.ville;
    infoUser.description = this.description;
    infoUser.linkedin = this.linkedin;
    infoUser.facebook = this.facebook;
    if (this.namePhoto) infoUser.photoUrl = this.user.urlPhoto;
    localStorage.setItem("UserInfo", JSON.stringify(infoUser));
    console.log("----------infoUser update localStorge------");
    console.log(infoUser);
  }
  ngOnInit() {
    if (JSON.parse(localStorage.getItem("UserInfo")).photoUrl === null)
      this.photo = "assets/img/" + "default-profile-pic.jpg";
    else
      this.photo =
        this.imageURL +
        `${JSON.parse(localStorage.getItem("UserInfo")).photoUrl}`;
    this.pays = JSON.parse(localStorage.getItem("UserInfo")).pays;
    this.firstname = JSON.parse(localStorage.getItem("UserInfo")).firstname;
    this.lastname = JSON.parse(localStorage.getItem("UserInfo")).lastname;
    this.metier = JSON.parse(localStorage.getItem("UserInfo")).metier;
    this.ville = JSON.parse(localStorage.getItem("UserInfo")).ville;
    this.description = JSON.parse(localStorage.getItem("UserInfo")).description;
    this.linkedin = JSON.parse(localStorage.getItem("UserInfo")).linkedin;
    this.facebook = JSON.parse(localStorage.getItem("UserInfo")).facebook;

    this.us
      .getUserById(JSON.parse(localStorage.getItem("UserInfo")).idUser)
      .subscribe(data => {
        this.usee = data;
        console.log(data);
      });
    // this.lastname=usee.lastname

    this.userEditForm = this.formBuilder.group({
      firstname: [
        "",
        [Validators.required, Validators.minLength(2), Validators.maxLength(15)]
      ],
      lastname: [
        "",
        [Validators.required, Validators.minLength(2), Validators.maxLength(15)]
      ],
      metier: [""],
      linkedin: [""],
      facebook: [""],
      description: [""],
      myFile: [""],
      ville: [""],
      pays: [""]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.userEditForm.controls;
  }
  preview() {
    this.valueProgress = 0;

    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = _event => {
      this.previewUrl = reader.result;
    };
  }
}
