import { ProfilService } from "./../../services/profils/profil.service";
import { Component, OnInit } from "@angular/core";
@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  connected = false;
  constructor(private ps: ProfilService) {
    this.isConnected();
  }

  ngOnInit() {
    this.isConnected();
  }
  isConnected() {
    if (this.ps.isLoggedIn()) {
      this.connected = true;
    }
  }
}
