import { Comment } from './../../models/comment';
import { CommentsService } from './../../services/comments/comments.service';
import { Router } from '@angular/router';
import { ProfilService } from 'src/app/services/profils/profil.service';
import { PublicationService } from './../../services/files/publication.service';
import { environment } from './../../../environments/environment';
import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FormGroup, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-video-card',
  templateUrl: './video-card.component.html',
  styleUrls: ['./video-card.component.css']
})
export class VideoCardComponent implements OnInit {
  @Input() publication;
  url: SafeResourceUrl;
  commentForm: FormGroup;
  comment = '';
  videosURL = environment.videosURL;
  imagesURL = environment.imagesURL;
  apiURL = environment.apiURl;
  photo = '../../../assets/img/default-profile-pic.jpg';
  idUserStorage = JSON.parse(localStorage.getItem('UserInfo')).id_user;
  userPhoto =
    environment.imagesURL+
    JSON.parse(localStorage.getItem('UserInfo')).photoUrl;
  visible = false;
  visibleListe = false;

  com: Comment;
  comments: any = [];

  constructor(
    public sanitizer: DomSanitizer,
    private ps: PublicationService,
    private us: ProfilService,
    private router: Router,
    private formBuilder: FormBuilder,
    private cs: CommentsService
  ) {}

  ngOnInit() {
    console.log('-------------------->' , this.publication)
    if (this.publication.photoUrl) {
      this.photo = this.imagesURL + this.publication.photoUrl;
    }
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(
      this.videosURL +
        this.publication.name +'.'+this.publication.extension
    );
    this.commentForm = this.formBuilder.group({
      comment: ['']
    });
    this.getComments(this.publication.idVideo);
  }
  show() {
    this.visible = !this.visible;
  }
  showList() {
    this.visibleListe = !this.visibleListe;
  }
  getProfile(idUser) {
    this.us.getProfil(idUser).subscribe(data => {
      console.log('dataaaaaaaa', data)

      if (data['user'].id_user) {
        if (data['user'].id_user === this.idUserStorage) {
          this.router.navigate(['profil']);
        } else {
          this.router.navigate(['profils/' + data['user'].id_user]);
        }
      } else { this.router.navigate(['home']); }
    });
  }
  getComments(idVideo) {
    this.cs.getComments(idVideo).subscribe(data => {
      this.comments = data['data'];
    });
  }
  deleteComment(id, idVideo){
    this.cs.deleteComment(id).subscribe(data => {
      if (data['msg'] !== null) {
        this.getComments(idVideo);
      }
    });
  }
  onKeydownEvent(event: KeyboardEvent, idVideo): void {
    // tslint:disable-next-line: deprecation
    if (event.keyCode === 13) {
      if (this.comment) {
        const commentaire = {
          comment: this.comment,
          idUser: this.idUserStorage,
          idVideo
        };

        this.cs.addComment(commentaire).subscribe(data => {
          console.log(data);
          this.comment = '';
          this.getComments(idVideo);
          this.visibleListe = true;
        });
      }
    }
  }

  delete(idVideo, url, extension) {
    const fileName = url + '.' + extension;
    this.ps.deletePublication(idVideo).subscribe(data => {
      console.log(data);
      this.ps.deleteVideo(fileName).subscribe(info => {
        console.log(info);
        location.reload();
      });
    });
  }
}
