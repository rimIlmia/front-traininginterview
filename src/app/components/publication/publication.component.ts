import { environment } from './../../../environments/environment';
import { PublicationService } from './../../services/files/publication.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpEventType } from '@angular/common/http';
@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: ['./publication.component.css']
})
export class PublicationComponent implements OnInit {
 
  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  valueProgress = 0;
  publicationForm: FormGroup;
  submitted = false;
  apiURL = environment.apiURl;
  question = '';
  nameVideo = '';
  activate = true;
  extension = '';
  extensionValidator = false;
  sizeVideo: any = null;
  messageSize = '';
  messageExtension = '';
  constructor(
    private httpClient: HttpClient,
    private formBuilder: FormBuilder,
    private us: PublicationService
  ) {}
  // chargement du fichier
  fileProgress(fileInput: any) {
    this.fileData = <File> fileInput.target.files[0];
    this.valueProgress = 0;
    this.nameVideo = this.fileData.name;
    // size file mo
    this.sizeVideo = this.fileData.size / 1048576;
    this.extension = this.nameVideo.split('.').pop();
    this.messageExtension = '';
    this.validationExtensionVideo();
    if (this.extensionValidator === true) {
      this.messageExtension = '';
    }

    if (this.validationSizeVideo() === true) {
      this.messageSize = '';
    }
  }

  validationExtensionVideo() {
    console.log('this.extension' + this.extension);
    switch (this.extension) {
      case 'mp4':
      case 'mov':
      case 'avi':
      case '3gp':
      case 'flv':
      case 'wmv':
        this.extensionValidator = true;
        break;
      default:
        this.extensionValidator = false;
    }
  }
  validationSizeVideo() {
    if (this.sizeVideo < 2 || this.sizeVideo > 150) {
      return false;
    } else {
      return true;
    }
  }

  onSubmit() {
    this.submitted = true;
    // verfication de l'extension du fichier
    this.validationExtensionVideo();
    console.log('this.extensionValidator' + this.extensionValidator);
    if (this.extensionValidator === false && this.submitted === true) {
      this.messageExtension =
        'Extensions vidéos acceptées sont: mp4, mov, avi, 3gp, flv,wmv';
    }
    // verification taille du fichier
    if (this.validationSizeVideo() === false && this.submitted === true) {
      this.messageSize = 'La taille de votre vidéo doit etre entre 5-150MO ';
    }
    const formData = new FormData();
    formData.append('myFile', this.fileData);
    // formulaire n'est pas valide
    if (
      this.publicationForm.invalid ||
      this.nameVideo === '' ||
      this.extensionValidator === false ||
      this.validationSizeVideo() === false
    ) {
      return;
    } else {
      // appel de l'api à la fonction upload qui utilise multer
      this.httpClient
        .post(this.apiURL + 'publication/video', formData, {
          reportProgress: true,
          observe: 'events'
        })
        .subscribe(events => {
          if (events.type === HttpEventType.UploadProgress) {

            // calcul pour la progressbar
            this.fileUploadProgress =
              Math.round((events.loaded / events.total) * 100) + '%';
            this.valueProgress = Math.round(
              (events.loaded / events.total) * 100
            );
            if (this.valueProgress === 100) {
              this.valueProgress = 0;
            }
          } else if (events.type === HttpEventType.Response) {
            this.fileUploadProgress = '';
            this.valueProgress = 0;
            if (this.valueProgress === 100) {
              this.valueProgress = 0;
            }
            // Quand le fichier est bien uploadé stoké ses données dans la BD
            const publication = {
              title: this.question,
              name: events.body['filename']
                .split('.')
                .slice(0, -1)
                .join('.'),
              extension: this.extension
            };
            // appel du service publication et ajout de la publication à la BD
            this.us
              .addPublication(publication)
              .subscribe(data => {console.log(data['msg'])
                                  location.reload();
              });
          }
        });
    }
  }
  ngOnInit() {
    this.publicationForm = this.formBuilder.group({
      question: ['', [Validators.required]],
      myFile: ['']
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.publicationForm.controls;
  }
  preview() {
    this.valueProgress = 0;

    // Show preview
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = _event => {
      this.previewUrl = reader.result;
    };
  }
}
