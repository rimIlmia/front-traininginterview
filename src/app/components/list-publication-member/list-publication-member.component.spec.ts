import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPublicationMemberComponent } from './list-publication-member.component';

describe('ListPublicationMemberComponent', () => {
  let component: ListPublicationMemberComponent;
  let fixture: ComponentFixture<ListPublicationMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPublicationMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPublicationMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
