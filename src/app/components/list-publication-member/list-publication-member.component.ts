import { VideoService } from "src/app/services/videos/video.service";
import { Component, OnInit, Input } from "@angular/core";
import { Subject } from "rxjs/internal/Subject";
import { PublicationService } from "src/app/services/files/publication.service";

@Component({
  selector: "app-list-publication-member",
  templateUrl: "./list-publication-member.component.html",
  styleUrls: ["./list-publication-member.component.css"]
})
export class ListPublicationMemberComponent implements OnInit {
  allvideos: any;
  interval: any;
  private unsubscribe: Subject<any> = new Subject();

  constructor(private vs: VideoService) {}
  @Input() idUser;
  ngOnInit() {
    console.log("255555555555555555555555555" + this.idUser);
    this.vs.getVideosByidUser(this.idUser).subscribe(data => {
      this.allvideos = data;
      console.log(data);
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
