import { PublicationService } from './../../services/files/publication.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-all-publication',
  templateUrl: './all-publication.component.html',
  styleUrls: ['./all-publication.component.css']
})
export class AllPublicationComponent implements OnInit {

  allvideos: any;
  interval: any;
  private unsubscribe: Subject<any> = new Subject();

  constructor(private publicationService: PublicationService) { }

  ngOnInit() {
    this.refreshData();
  }
  refreshData() {
    this.publicationService.getAllPublication()
      .subscribe(data => {
        this.allvideos = data["data"];
      });
  }
  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

}
