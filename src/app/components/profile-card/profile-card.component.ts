import { environment } from './../../../environments/environment';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.css']
})
export class ProfileCardComponent implements OnInit {
  @Input() user;
  photo = '../../../assets/img/default-profile-pic.jpg';
  idUserStorage = JSON.parse(localStorage.getItem('UserInfo')).idUser;
  myphoto = environment.imagesURL;
  skills: any;
  constructor(private router: Router) {
  }
  redirection(){
    this.router.navigate(['/profil/edit']);
  }

  ngOnInit() {
    this.skills = this.user.skills;
    console.log('experience', this.user.experience);
  }
}
