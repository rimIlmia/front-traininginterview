import { ProfilService } from "./../../services/profils/profil.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
declare let $: any;
@Component({
  selector: "app-navbar-cnx",
  templateUrl: "./navbar-cnx.component.html",
  styleUrls: ["./navbar-cnx.component.css"]
})
export class NavbarCnxComponent implements OnInit {
  constructor(private ps: ProfilService, private router: Router) {}
firstname=""
lastname=""
  filter = "Metier";
  valueItem(valeur) {
    this.filter = valeur;
  }

  logOut() {
    this.ps.logOut();
    this.router.navigate(["/connexion"]);
  }
  ngOnInit() {
    this.lastname = JSON.parse(localStorage.getItem("UserInfo")).lastname;
    this.firstname = JSON.parse(localStorage.getItem("UserInfo")).firstname;
  }
}
