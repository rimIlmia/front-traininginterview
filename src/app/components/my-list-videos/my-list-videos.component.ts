import { VideoService } from "./../../services/videos/video.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subject } from "rxjs";

@Component({
  selector: "app-my-list-videos",
  templateUrl: "./my-list-videos.component.html",
  styleUrls: ["./my-list-videos.component.css"]
})
export class MyListVideosComponent implements OnInit, OnDestroy {
  allvideos: any;
  interval: any;
  private unsubscribe: Subject<any> = new Subject();

  constructor( private vs: VideoService) {}
  
  ngOnInit() {
    this.refreshData();
    // if (this.interval) {
    //   clearInterval(this.interval);
    // }
    // this.interval = setInterval(() => {
    //   this.refreshData();
    // }, 10000);
  }
  refreshData() {
    this.vs
      .getVideosByidUser(JSON.parse(localStorage.getItem("UserInfo")).idUser)
      .subscribe(data => {
        this.allvideos = data;
      });
  }
  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
