import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProfilService } from '../services/profils/profil.service';

@Component({
  selector: 'app-page-publication',
  templateUrl: './page-publication.component.html',
  styleUrls: ['./page-publication.component.css']
})
export class PagePublicationComponent implements OnInit {
  user: any;
  constructor(private us: ProfilService, private router: Router) { }

  ngOnInit() {
    // initialisation
    console.log('token');

    this.us
      .getUserById(JSON.parse(localStorage.getItem('UserInfo')).id_user)
      .subscribe(data => {
        // tslint:disable-next-line: no-string-literal
        this.user = data['user'];
        console.log(this.user);
      }, error => {
      } );
  }

}
