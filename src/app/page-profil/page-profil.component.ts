import { ProfilService } from "./../services/profils/profil.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-page-profil",
  templateUrl: "./page-profil.component.html",
  styleUrls: ["./page-profil.component.css"]
})
export class PageProfilComponent implements OnInit {
  user: any ;
  constructor(private us: ProfilService) {}
  ngOnInit() {
    // initialisation 
    this.us
      .getUserById(JSON.parse(localStorage.getItem("UserInfo")).idUser)
      .subscribe(data => {
        this.user = data["user"];
      });
  }
}
