import { ProfilService } from "./../services/profils/profil.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { User } from "../models/user";

import { Observable } from "rxjs";
import { Router } from "@angular/router";
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalOptions
} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-page-inscription",
  templateUrl: "./page-inscription.component.html",
  styleUrls: ["./page-inscription.component.css"]
})
export class PageInscriptionComponent implements OnInit {
  title = "TrainingInterview";
  closeResult: string;
  modalOptions: NgbModalOptions;
  lastname = "";
  inscriptionForm: FormGroup;
  password = "";
  repassword = "";
  email = "";
  firstname = "";
  submitted = false;
  user: User;
  messageError = "";
  errorMsg = false;
  modalReference: any;
  constructor(
    private router: Router,
    private profilService: ProfilService,
    private fb: FormBuilder,
    public modalService: NgbModal
  ) {
    this.modalOptions = {
      backdrop: "static",
      backdropClass: "customBackdrop"
    };
  }

  inscription(content) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.inscriptionForm.invalid) {
      return;
    } else {
      // valid form
      this.user = {
        firstname: this.firstname,
        lastname: this.lastname,
        email: this.email,
        password: this.password,
        urlPhoto: "default-profile-pic.jpg"
      };
      // create user
      this.profilService.inscription(this.user).subscribe(data => {
        if (data["success"]) {
          this.open(content);
        }
      }, err => {
          this.errorMsg = true;
          this.messageError = err.error["msg"];
        }
      );
    }
  }

  ngOnInit() {
    // validation form
    this.inscriptionForm = this.fb.group(
      {
        firstname: [
          "",
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(15)
          ]
        ],
        lastname: [
          "",
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(15)
          ]
        ],
        email: ["", [Validators.required, Validators.email]],
        password: [
          "",
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(12),
            Validators.pattern("^(?=.*\\d).{8,12}$")
          ]
        ],
        repassword: ["", Validators.required]
      },
      {
        validator: this.MustMatch("password", "repassword")
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.inscriptionForm.controls;
  }
  //Modal
  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
  open(content) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }
  close() {
    this.modalReference.close();

    this.router.navigate(["connexion"]);
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
