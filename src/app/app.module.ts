import { AuthGuard } from "./guards/auth.guard";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, LOCALE_ID } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavbarHomeComponent } from "./components/navbar-home/navbar-home.component";
import { NavbarCnxComponent } from "./components/navbar-cnx/navbar-cnx.component";
import { MainHomeComponent } from "./components/main-home/main-home.component";
import { PageProfilComponent } from "./page-profil/page-profil.component";
import { PageConnexionComponent } from "./page-connexion/page-connexion.component";
import { PageInscriptionComponent } from "./page-inscription/page-inscription.component";
import { PageAccueilComponent } from "./page-accueil/page-accueil.component";
import { PageAproposComponent } from "./page-apropos/page-apropos.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { HttpClientModule } from "@angular/common/http";
import { ProfilService } from "./services/profils/profil.service";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { IsNotConnectedGuard } from "./guards/is-not-connected.guard";
import { CardComponent } from "./components/card/card.component";
import { PublicationComponent } from "./components/publication/publication.component";
import { ProfileCardComponent } from "./components/profile-card/profile-card.component";
import { BlogComponent } from "./components/blog/blog.component";
import { VideoCardComponent } from "./components/video-card/video-card.component";
import { MyListVideosComponent } from "./components/my-list-videos/my-list-videos.component";
import { registerLocaleData } from "@angular/common";
import localeFr from "@angular/common/locales/fr";
import { EditProfilComponent } from "./components/edit-profil/edit-profil.component";
import { PageEditProfilComponent } from "./page-edit-profil/page-edit-profil.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PagePublicationComponent } from './page-publication/page-publication.component';
import { AllPublicationComponent } from './components/all-publication/all-publication.component';
import { PageMemberComponent } from './page-member/page-member.component';
import { ListPublicationMemberComponent } from './components/list-publication-member/list-publication-member.component';
import { MessageComponent } from './components/message/message.component';
// the second parameter 'fr' is optional
registerLocaleData(localeFr, "fr");
@NgModule({
  declarations: [
    AppComponent,
    NavbarHomeComponent,
    NavbarCnxComponent,
    MainHomeComponent,
    PageProfilComponent,
    PageConnexionComponent,
    PageInscriptionComponent,
    PageAccueilComponent,
    PageAproposComponent,
    NotFoundComponent,
    NavbarComponent,
    CardComponent,
    PublicationComponent,
    ProfileCardComponent,
    BlogComponent,
    VideoCardComponent,
    MyListVideosComponent,
    EditProfilComponent,
    PageEditProfilComponent,
    PagePublicationComponent,
    AllPublicationComponent,
    PageMemberComponent,
    ListPublicationMemberComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [
    AuthGuard,
    ProfilService,
    IsNotConnectedGuard,
    { provide: LOCALE_ID, useValue: "fr-CA" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
