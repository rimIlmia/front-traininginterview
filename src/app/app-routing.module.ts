import { IsNotConnectedGuard } from "./guards/is-not-connected.guard";
import { AuthGuard } from "./guards/auth.guard";
import { PageProfilComponent } from "./page-profil/page-profil.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { PageInscriptionComponent } from "./page-inscription/page-inscription.component";
import { PageConnexionComponent } from "./page-connexion/page-connexion.component";
import { PageAproposComponent } from "./page-apropos/page-apropos.component";
import { PageAccueilComponent } from "./page-accueil/page-accueil.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PageEditProfilComponent } from "./page-edit-profil/page-edit-profil.component";
import { PagePublicationComponent } from './page-publication/page-publication.component';
import { PageMemberComponent } from './page-member/page-member.component';

const routes: Routes = [
  {
    path: "",
    component: PageAccueilComponent,
    canActivate: [IsNotConnectedGuard]
  },
  { path: "apropos", component: PageAproposComponent },
  {
    path: "connexion",
    component: PageConnexionComponent,
    canActivate: [IsNotConnectedGuard]
  },
  {
    path: "inscription",
    component: PageInscriptionComponent,
    canActivate: [IsNotConnectedGuard]
  },
  {
    path: "profil", component: PageProfilComponent, canActivate: [AuthGuard], children: []
    },
  {
    path: "profil/edit",
    component: PageEditProfilComponent,
    canActivate: [AuthGuard]
  }, 
    {path: "profils/:id", component: PageMemberComponent, canActivate: [AuthGuard]},
  { path: "home", component: PagePublicationComponent, canActivate: [AuthGuard] },

 

  { path: "**", component: NotFoundComponent }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
