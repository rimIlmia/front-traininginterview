import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user';
import * as AppUtil from '../../common/app.util';
import { LoginUser } from '../../models/loginUser';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {
  apiURL = environment.apiURl;
  data: any;
  token = localStorage.getItem('AuthToken');
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token
      })
    };
  constructor(private httpClient: HttpClient) {

  }
// get user
  getUserById(id) {
    return this.httpClient.get(`${this.apiURL}user/` + id, this.httpOptions);
  }

// get details user by id video
  getProfil(idUser) {
    return this.httpClient.get(`${this.apiURL}user/` + idUser, this.httpOptions);
  }
// create user
  inscription(user: User) {
    return this.httpClient.post<User>(`${this.apiURL}register`, user);
  }
// update profile
  updateProfile(user: User) {
    return this.httpClient.put<User>(
      `${this.apiURL}user/` + user.idUser,
      user, this.httpOptions
    );
  }
  /***authentification***/
  connexion(user: LoginUser) {
    return this.httpClient.post<LoginUser>(`${this.apiURL}login`, user);
  }
// get token
  // profil(token) {
  //   return this.httpClient.get<User>(`${this.apiURL}user/me/`,  this.httpOptions);
  // }
  // save details user in local storage
  saveUserData(token, user) {
    localStorage.setItem(AppUtil.AUTH_TOKEN, token);
    localStorage.setItem(AppUtil.USER_INFO, JSON.stringify(user));
  }
  // verify user authenfication
  isLoggedIn(): boolean {
    if (localStorage.getItem(AppUtil.AUTH_TOKEN)) {
      return true;
    }
    return false;
  }
  // deconnexion logout
  logOut() {
    localStorage.removeItem(AppUtil.AUTH_TOKEN);
    localStorage.removeItem(AppUtil.USER_INFO);
  }

}
