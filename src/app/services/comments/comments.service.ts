import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  apiURL = environment.apiURl + 'comment';

  constructor(private httpClient: HttpClient) {}
  // List Comments by idVideo
  getComments(idVideo) {
    return this.httpClient.get(`${this.apiURL}/publication/` + idVideo);
  }
  // create comment
  addComment(comment) {
    return this.httpClient.post(`${this.apiURL}`, comment);
  }
 // delete comment by id comment
  deleteComment(id) {
    return this.httpClient.request('DELETE', `${this.apiURL}/` + id);
  }
}
