import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class VideoService {

  apiURL = environment.imagesURL + 'publication';
  token = localStorage.getItem('AuthToken');
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token
      })
    };

  constructor(private httpClient: HttpClient) {}
  // get publication by iduser
  getVideosByidUser(id) {
    return this.httpClient.get(`${this.apiURL}` + id, this.httpOptions);
  }
}
