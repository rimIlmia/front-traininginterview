import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PublicationService {
  uploadForm: FormGroup;

  apiURL = environment.apiURl + 'publication';
  data: any;
  token = localStorage.getItem('AuthToken');
  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + this.token
      })
    };
  constructor(private httpClient: HttpClient) {}
  // List Publication
  getAllPublication() {
    return this.httpClient.get(
      `${this.apiURL}`, this.httpOptions
    );
  }
  // publication by id user
  getPublicationMember(idUser) {

    return this.httpClient.get(
      `${this.apiURL}`, this.httpOptions
    );
  }
// create publication
  addPublication(publication) {
    const idUser = JSON.parse(localStorage.getItem('UserInfo')).id_user;
    return this.httpClient.post(
      `${this.apiURL}/user/` + idUser,
      publication, this.httpOptions
    );
  }
  // delete publication
  deletePublication(idVideo) {
    return this.httpClient.delete(
      `${this.apiURL}` + idVideo, this.httpOptions
    );
  }
  deleteVideo(fileName) {
    return this.httpClient.delete(
      `${this.apiURL}` + fileName + '/', this.httpOptions
    );
  }

}
