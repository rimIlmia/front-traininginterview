import { Component, OnInit } from "@angular/core";
import { ProfilService } from "./../services/profils/profil.service";
import {
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";

import { LoginUser } from "../../app/models/loginUser";
import { Router } from "@angular/router";

@Component({
  selector: "app-page-connexion",
  templateUrl: "./page-connexion.component.html",
  styleUrls: ["./page-connexion.component.css"]
})
export class PageConnexionComponent implements OnInit {
  connexionForm: FormGroup;
  password = "";
  email = "";
  submitted = false;
  user: LoginUser;
  messageError = "";
  errorMsg = false;

  constructor(
    private router: Router,
    private profilService: ProfilService,
    private fb: FormBuilder
  ) {}

  connexion() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.connexionForm.invalid) {
      return;
    } else {
      //valid form
      this.user = {
        email: this.email,
        password: this.password,
      };
      // call service  profile  authentification
      this.profilService.connexion(this.user).subscribe(data => {
        
           this.profilService.saveUserData(data["token"], data["user"]);
              this.router.navigate(["/home"]);
      
      }, err => {
           // display error 
          this.errorMsg = true;
          this.messageError = err.error["msg"];
      });
    }
  }

  ngOnInit() {
    this.connexionForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required]]
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.connexionForm.controls;
  }
}
