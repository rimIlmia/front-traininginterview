import { ProfilService } from "./../services/profils/profil.service";
import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class IsNotConnectedGuard implements CanActivate {
  constructor(private ps: ProfilService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!this.ps.isLoggedIn()) {
      return true;
    }
    //Page  non accessible quand l'utilisateur est connecté exemple (inscription)
    this.router.navigate(["/home"]);
    return false;
  }
}
