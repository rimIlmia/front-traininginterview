import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";

import { ProfilService } from "../services/profils/profil.service";

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: ProfilService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.isLoggedIn();

    if (currentUser) {
      return true;
    }
    // not logged in so redirect to login page with the return url
    
    // page qui necessite authentification pour y accéder

    this.router.navigate(["/connexion"]);
    return false;
  }
}
